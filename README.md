# BioJSRegistry2ELIXIRRegistry

BioJSRegistry2ELIXIRRegistry is a tool for requesting all BioJS-Components from the [BioJS-Registry](http://registry.biojs.net/client/#/) and to push them to the [ELIXIR-Registry](http://elixir-registry-demo.cbs.dtu.dk). It's written in Java, using Maven and requests JSON from the [BioJS-Registry](http://registry.biojs.net/client/#/), uses [Jackson](http://jackson.codehaus.org) to generate JAVA-Objects out of it. Those JAVA-Objects going to be costomized and back converted to JSON to registeres those at the [ELIXIR-Registry](http://elixir-registry-demo.cbs.dtu.dk).

### Installation

* To start with it, clone the project to your local machine
* Build a package out of it to get a executable jar-file:
```
mvn clean package
```
* jar-file will be in the target-folder

### Usage
* use "BioJSRegistry2ELIXIRRegistry-1.0-SNAPSHOT-jar-with-dependencies.jar"
```
java -jar BioJSRegistry2ELIXIRRegistry-1.0-SNAPSHOT-jar-with-dependencies.jar <username> <password>
```

###Which information is used from the BioJS-Registry?
As basic resource, it uses the JSON-String from the [BioJS-Registry](http://registry.biojs.net/client/#/). But not all the information is used for the [ELIXIR-Registry](http://elixir-registry-demo.cbs.dtu.dk). Here are the used attributes:

* Name
* Author
* Version
* Homepage
* Contact
* Keywords

###Which information is required from the [ELIXIR-Registry](http://elixir-registry-demo.cbs.dtu.dk)?
Additional to the given information from the BioJS-Registry, it is needed to provide a minimum of mandatory fields for 
the [ELIXIR-Registry](http://elixir-registry-demo.cbs.dtu.dk). This are the mandatory fields:

* Name
* Homepage 
* Description 
* Resource type
* Interface type
* Topics 
* Functions
* Input types
* Output types
* Contact 

Not all of them can be covered by information from the [BioJS-Registry](http://registry.biojs.net/client/#/). All of the missing attributes and information are part of a given ontology's. This are the given ontology's:

* EDAM 
* CBS-DTU

Those information are submitted on the top level value of each ontology-branch. After uploading the tool to the [ELIXIR-Registry](http://elixir-registry-demo.cbs.dtu.dk), all the default information can be specified on the ELIXIR-Registry web-interface.

### How does the update mechanism work?
There are two kinds of tools: tools who already exist and tools who are new. 
####Tools who are already existing
For those tools only the information from the BioJS-Registry are going to be overridden, to keep maybe customized fields
like Topic or Cost.
####Tools who are new
For those tools all the missing data will be default, that means only the BioJS-Registry information will be in place and 
the other mandatory fields are seated up by top level value, the rest is null or empty.

###Workflow

1. Login on ELIXIR-Registry and receive login token for http-requests(POST, DELETE)
2. Request all BioJS-Components as JSON-String
3. Request all ELIXIR-Registry-Tools, which are related to the username
4. Map BioJS-Components and ELIXIR-Registry-Tools, using Jackson, to Java-Objects
5. Check if BioJS-Component is already in ELIXIR-Registry registered:

  * If yes, copy all values of the ELIXIR-Registry-Tool, which are not given from the BioJS-Component, to the BioJS-Component.
  * If not, create a new default ELIXIR-Registry-Tool.
 
6. Send each ELIXIR-Registry-Tool to the ELIXIR-Registry

### Troubleshooting

###Dependencies

* [Jackson Annotations](http://mvnrepository.com/artifact/com.fasterxml.jackson.core/jackson-annotations)
* [Jackson Databind](http://mvnrepository.com/artifact/com.fasterxml.jackson.core/jackson-databind)
* [Apache HttpClient](http://mvnrepository.com/artifact/org.apache.httpcomponents/httpclient)
* [JSON In Java](http://mvnrepository.com/artifact/org.json/json)
* [Apache Commons Logging](http://mvnrepository.com/artifact/commons-logging/commons-logging)

###Others
####How to create all mapping classes easy and in one go?
* Using [jsonschema2pojo](http://www.jsonschema2pojo.org), a nice tool to convert JSON to Java-Classes
