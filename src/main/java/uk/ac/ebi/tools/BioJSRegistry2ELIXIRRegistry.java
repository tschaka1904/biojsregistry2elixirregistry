package uk.ac.ebi.tools;

import com.martiansoftware.jsap.JSAPResult;
import uk.ac.ebi.tools.mapping.registry.ElixirRegistryToolFactory;
import uk.ac.ebi.tools.mapping.registry.MappingHandler;
import uk.ac.ebi.tools.mapping.registry.biojs.BioJSComponent;
import uk.ac.ebi.tools.mapping.registry.elixir.ElixirRegistryTool;
import uk.ac.ebi.tools.request.registry.RequestHandler;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * @author Maximilian Koch <mkoch@ebi.ac.uk>
 */
public class BioJSRegistry2ELIXIRRegistry {
    /**
     * Counter variables, for statistical output
     */
    public static int registeredTools = 0;
    public static int failedOnRegisterTools = 0;

    public static void main(String[] args) {
        JSAPHandler jsapHandler = new JSAPHandler();

        JSAPResult jsapResult = jsapHandler.ArgumentHandler(args);
        LoginProperties.getLoginProperties().setUsername(jsapResult.getString("username"));
        LoginProperties.getLoginProperties().setPassword(jsapResult.getString("password"));


        System.out.println("Starting process to upload BioJS-Components to ELIXIR-Registry");
        System.out.println("Resources:");
        System.out.println("ELIXIR-Registry: http://elixir-registry-demo.cbs.dtu.dk");
        System.out.println("BioJS-Registry (JSON-Version): http://workmen.biojs.net/all?short=1");
        System.out.println("______________________________________");
        System.out.println("");

        /**
         * Login token, which is requested from ELIXIR-Registry
         */
        String loginToken = RequestHandler.getRegistryLoginToken();

        /**
         * JSON-String off all BioJS-Components, based on the BioJS-Registry
         */
        String bioJSComponentsJSON = RequestHandler.getBioJSComponents();
        /**
         * JSON-String off all registered ELIXIR-Tools, who are related to that Username
         */
        String allElixirToolsJSON = RequestHandler.getAllElixirTools();
        /**
         * Collection of all BioJS-Components, mapped to Java-Object
         */
        Set<BioJSComponent> allBioJSComponents = MappingHandler.biojsComponentJSONToObject(bioJSComponentsJSON);
        /**
         * Set of ELIXIR-Tools, based on all BioJS-Components and mapped to ElixirRegistryTool by default values
         */
        Set<ElixirRegistryTool> defaultElixirTools = ElixirRegistryToolFactory.createDefaultElixirTools(allBioJSComponents);
        /**
         * Map of ElixirRegistryTools, for having a better update functionality.
         * //TODO: Using refactor
         */
        Map<String, ElixirRegistryTool> elixirToolMap = new HashMap<String, ElixirRegistryTool>();
        if (!allElixirToolsJSON.equals("{\"status\": \"error\", \"text\": \"tool not found\"}")) {
            elixirToolMap.putAll(MappingHandler.convertElixirToolJSONTOElixirToolObject(allElixirToolsJSON));
        }
        /**
         * Collection of ElixirRegistryTools, containing all tools, who are supposed to be registered
         */
        Set<ElixirRegistryTool> newElixirTools = ElixirRegistryToolFactory.getNewElixirTools(defaultElixirTools, elixirToolMap);
        /**
         * Always before tools start to be submitted, all old tools have to be deleted from ELIXIR-Registry
         */
        RequestHandler.deleteAllElixirRegistryTools(loginToken);
        /**
         * For every given ELIXIR-Tool in newElixirTools, convert it back to JSON and registered it to the
         * ELIXIR-Register.
         */
        for (ElixirRegistryTool newElixirTool : newElixirTools) {
            String jsonString = MappingHandler.convertElixirToolToJSON(newElixirTool);
            boolean uploadSuccess = RequestHandler.sendElixirRegistryTool(loginToken, jsonString);
            if (uploadSuccess) {
                registeredTools += 1;
                System.out.println(newElixirTool.getName() + " has been registered");
            } else {
                failedOnRegisterTools += 1;
                System.err.println(newElixirTool.getName() + " couldn't been registered");
            }
        }
        System.out.println("______________________________________");
        System.out.println("");
        System.out.println("Registered tools: " + registeredTools);
        System.out.println("Failed tools: " + failedOnRegisterTools);
        System.out.println("");
        System.out.println("Upload completed");
    }
}
