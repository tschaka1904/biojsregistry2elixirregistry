package uk.ac.ebi.tools;

import com.martiansoftware.jsap.FlaggedOption;
import com.martiansoftware.jsap.JSAP;
import com.martiansoftware.jsap.JSAPException;
import com.martiansoftware.jsap.JSAPResult;

/**
 * Created by Maximilian Koch (mkoch@ebi.ac.uk).
 */
public class JSAPHandler {

    public JSAPResult ArgumentHandler(String[] args) {
        JSAP jsap = new JSAP();

        FlaggedOption opt1 = new FlaggedOption("username")
                .setStringParser(JSAP.STRING_PARSER)
                .setRequired(true)
                .setShortFlag('u')
                .setLongFlag("username");
        opt1.setHelp("the registry user");

        FlaggedOption opt2 = new FlaggedOption("password")
                .setStringParser(JSAP.STRING_PARSER)
                .setRequired(true)
                .setShortFlag('p')
                .setLongFlag("password");
        opt2.setHelp("the password to connect to the registry");

        try {
            jsap.registerParameter(opt1);
            jsap.registerParameter(opt2);
        } catch (JSAPException e) {
            e.printStackTrace();
        }

        JSAPResult jsapResult = jsap.parse(args);

        if (!jsapResult.success()) {
            System.err.println();
            System.err.println("Usage: java "
                    + BioJSRegistry2ELIXIRRegistry.class.getName());
            System.err.println("                "
                    + jsap.getUsage());
            System.err.println();
            System.err.println(jsap.getHelp());
            System.exit(1);
        }
        return jsapResult;
    }
}
