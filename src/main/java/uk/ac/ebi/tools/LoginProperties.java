package uk.ac.ebi.tools;

/**
 * Created by maximiliankoch on 20/11/14.
 */
public class LoginProperties {
    private static final LoginProperties loginProperties = new LoginProperties();

    private String username;
    private String password;

    private LoginProperties() {
    }

    public static LoginProperties getLoginProperties() {
        return loginProperties;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
