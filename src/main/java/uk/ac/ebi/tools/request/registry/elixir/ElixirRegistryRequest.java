package uk.ac.ebi.tools.request.registry.elixir;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import uk.ac.ebi.tools.LoginProperties;

import java.io.IOException;

/**
 * @author Maximilian Koch <mkoch@ebi.ac.uk>
 */
public class ElixirRegistryRequest {
    /**
     * base url for request against ELIXIR-Registry, to receive a JSON-Response
     */
    private static final String elixirRegistryURL = "https://elixir-registry.cbs.dtu.dk/api";
    /**
     * POST-Request for receiving loginToken
     *
     * @return response of request
     * @throws IOException
     */
    public static HttpResponse requestLoginToken() throws IOException {
        HttpResponse response;
        String requestBody = getLoginJSONString(LoginProperties.getLoginProperties());
        HttpPost httpPost = new HttpPost(elixirRegistryURL + "/auth/login");
        ContentType contentType = ContentType.create("application/json");
        StringEntity stringEntity = new StringEntity(requestBody, contentType);
        httpPost.setEntity(stringEntity);
        response = closeableHttpClient().execute(httpPost);
        closeableHttpClient().close();
        return response;
    }
    /**
     * POST-Request for registering a tool at ELIXIR-Registry
     *
     * @param loginToken login token for authentication
     * @param jsonString JSON-String of ElixirRegistryTool
     * @return response
     * @throws IOException
     */
    public static HttpResponse sendToRegistry(String loginToken, String jsonString) throws IOException {
        HttpResponse response;
        HttpPost httpPost = new HttpPost(elixirRegistryURL + "/tool");
        ContentType contentType = ContentType.create("application/json");
        httpPost.setHeader("Authorization", "Token " + loginToken);
        StringEntity stringEntity = new StringEntity(jsonString, contentType);
        httpPost.setEntity(stringEntity);
        response = closeableHttpClient().execute(httpPost);
        closeableHttpClient().close();
        return response;
    }
    /**
     * GET-Request for requesting all ELIXIR-Tools in the ELIXIR-Registry, based on username
     * @return response
     * @throws IOException
     */
    public static HttpResponse requestAllElixirTools() throws IOException {
        HttpResponse response;
        HttpGet httpGet = new HttpGet(elixirRegistryURL + "/tool/" + LoginProperties.getLoginProperties().getUsername());
        response = closeableHttpClient().execute(httpGet);
        closeableHttpClient().close();
        return response;
    }
    /**
     * DELETE-Request for deleting all ELIXIR-Tools form ELIXIR-Registry
     * @param loginToken login Token for authentication
     * @return response
     * @throws IOException
     */
    public static HttpResponse deleteAllElixirTools(String loginToken) throws IOException {
        HttpResponse response;
        HttpDelete httpDelete = new HttpDelete(elixirRegistryURL + "/tool/" + LoginProperties.getLoginProperties().getUsername());
        httpDelete.setHeader("Authorization", "Token " + loginToken);
        response = closeableHttpClient().execute(httpDelete);
        closeableHttpClient().close();
        return response;
    }
    /**
     * Convert LoginProperties to JSON-String
     * @param login LoginProperties
     * @return login JSON-String
     */
    private static String getLoginJSONString(LoginProperties login) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(login);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }
    /**
     * HttpClient
     *
     * @return Http CloseableHttpClient
     */
    private static CloseableHttpClient closeableHttpClient() {
        return HttpClients.createDefault();
    }
}
