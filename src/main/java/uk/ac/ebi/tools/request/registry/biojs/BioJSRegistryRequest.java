package uk.ac.ebi.tools.request.registry.biojs;


import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import java.io.IOException;

/**
 * @author Maximilian Koch <mkoch@ebi.ac.uk>
 */
public class BioJSRegistryRequest {
    /**
     * base url for request against BioJS-Registry, to receive a JSON-Response
     */
    private static final String bioJSRegistryURL = "http://workmen.biojs.net";
    /**
     * GET-Request for receiving all BioJS-Components on BioJS-Registry
     * @return response of request
     * @throws IOException
     */
    public static HttpResponse requestBioJSRegistry() throws IOException {
        String url = bioJSRegistryURL + "/all?short=1";
        HttpGet httpGet = new HttpGet(url);
        HttpResponse response;
        response = closeableHttpClient().execute(httpGet);
        closeableHttpClient().close();
        return response;
    }
    /**
     * HttpClient
     * @return Http CloseableHttpClient
     */
    private static CloseableHttpClient closeableHttpClient() {
        return HttpClients.createDefault();
    }
}
