package uk.ac.ebi.tools.request.registry;


import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import uk.ac.ebi.tools.request.registry.biojs.BioJSRegistryRequest;
import uk.ac.ebi.tools.request.registry.elixir.ElixirRegistryRequest;

import java.io.IOException;

/**
 * @author Maximilian Koch <mkoch@ebi.ac.uk>
 */
public class RequestHandler {
    /**
     * Handle the response of the request of all BioJS-Components from BioJS-Regstry
     * @return JSON-String
     */
    public static String getBioJSComponents() {
        String jsonString = null;
        try {
            HttpResponse httpResponse = BioJSRegistryRequest.requestBioJSRegistry();
            HttpEntity entity = httpResponse.getEntity();
            jsonString = EntityUtils.toString(entity, "UTF-8");
        } catch (IOException e) {
            System.err.println("Error on requesting BioJS-Registry ");
            e.printStackTrace();
            System.exit(0);
        }
        return jsonString;
    }
    /**
     * Handle the response of the request for the login token from the ELIXIR-Registry
     * @return String loginToken
     */
    public static String getRegistryLoginToken() {
        String responseBody = null;
        HttpResponse httpResponse = null;
        try {
            httpResponse = ElixirRegistryRequest.requestLoginToken();
        } catch (IOException e) {
            System.err.println("Failure on requesting login token from ELIXIR-Registry");
            e.printStackTrace();
            System.exit(0);
        }
        //TODO: check if those two elements can't be merged?
        if (httpResponse.getStatusLine().getStatusCode() != 200) {
            System.err.println("Failure on requesting login token from ELIXIR-Registry");
            System.err.println("Response: " + httpResponse);
            System.exit(0);
        }
        HttpEntity entity = httpResponse.getEntity();
        try {
            responseBody = EntityUtils.toString(entity, "UTF-8");
        } catch (IOException e) {
            System.err.println("Error on reading response body");
            System.err.println("Response: " + httpResponse);
            e.printStackTrace();
            System.exit(0);
        }
        return String.valueOf(new JSONObject(responseBody).get("token"));
    }
    /**
     * Handle response of the request to register a ELIXIR-Tool at the ELIXIR-Register
     * @param loginToken token for authentication
     * @param jsonString JSON-String of ElixirRegistryTool
     * @return Depending on response returns true or false
     */
    public static boolean sendElixirRegistryTool(String loginToken, String jsonString) {
        HttpResponse httpResponse = null;
        try {
            httpResponse = ElixirRegistryRequest.sendToRegistry(loginToken, jsonString);
        } catch (IOException e) {
            System.err.println("Failure on sending tool to Elixir-Registry");
            e.printStackTrace();
            System.exit(0);
        }
        /**
         * On status code 201 everything went well!
         */
        if (httpResponse.getStatusLine().getStatusCode() == 201) {
            return true;
        } else {
            System.err.println("Error on sending data to ELIXIR-Registry. ");
            System.err.println("Response: " + httpResponse);
            return false;
        }
    }
    /**
     * Handle response of request all ElixirTools, based on this username.
     * @return JSON-String of all Tools in ELIXIR-Registry
     */
    public static String getAllElixirTools() {
        String jsonString = null;
        HttpResponse httpResponse;
        try {
            httpResponse = ElixirRegistryRequest.requestAllElixirTools();
            HttpEntity entity = httpResponse.getEntity();
            jsonString = EntityUtils.toString(entity, "UTF-8");
        } catch (IOException e) {
            System.err.println("Error on requesting BioJS-Registry ");
            e.printStackTrace();
            System.exit(0);
        }
        return jsonString;
    }
    /**
     * Handle response of request to delete all tools, based on this username, from ELIXIR-Registry
     * @param loginToken login token for authentication
     * @return after finish return true
     */
    public static boolean deleteAllElixirRegistryTools(String loginToken) {
        HttpResponse httpResponse;
        try {
            httpResponse = ElixirRegistryRequest.deleteAllElixirTools(loginToken);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //TODO: Use http status code instead of a string!
        /**
         * Because the delete functionality doesn't work properly (should delete everything, on one request!)
         * we need this little workaround
         */
        while (!getAllElixirTools().equals("{\"status\": \"error\", \"text\": \"tool not found\"}")) {
            deleteAllElixirRegistryTools(loginToken);
        }
        return true;
    }
}
