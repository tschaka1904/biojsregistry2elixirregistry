package uk.ac.ebi.tools.mapping.registry.elixir;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.annotation.Generated;
import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
        "uri",
        "term"
})
public class Maturity {

    @JsonProperty("uri")
    private String uri;
    @JsonProperty("term")
    private String term;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * @return The uri
     */
    @JsonProperty("uri")
    public String getUri() {
        return uri;
    }

    /**
     * @param uri The uri
     */
    @JsonProperty("uri")
    public void setUri(String uri) {
        this.uri = uri;
    }

    /**
     * @return The term
     */
    @JsonProperty("term")
    public String getTerm() {
        return term;
    }

    /**
     * @param term The term
     */
    @JsonProperty("term")
    public void setTerm(String term) {
        this.term = term;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Maturity maturity = (Maturity) o;

        if (term != null ? !term.equals(maturity.term) : maturity.term != null) return false;
        if (uri != null ? !uri.equals(maturity.uri) : maturity.uri != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = uri != null ? uri.hashCode() : 0;
        result = 31 * result + (term != null ? term.hashCode() : 0);
        return result;
    }
}
