package uk.ac.ebi.tools.mapping.registry.elixir;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.annotation.Generated;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
        "uri",
        "term"
})
public class FunctionName {

    @JsonProperty("uri")
    private String uri;
    @JsonProperty("term")
    private String term;

    public FunctionName(@JsonProperty("uri") String uri, @JsonProperty("term") String term) {
        this.uri = uri;
        this.term = term;
    }

    /**
     * @return The uri
     */
    @JsonProperty("uri")
    public String getUri() {
        return uri;
    }

    /**
     * @param uri The uri
     */
    @JsonProperty("uri")
    public void setUri(String uri) {
        this.uri = uri;
    }

    /**
     * @return The term
     */
    @JsonProperty("term")
    public String getTerm() {
        return term;
    }

    /**
     * @param term The term
     */
    @JsonProperty("term")
    public void setTerm(String term) {
        this.term = term;
    }

}
