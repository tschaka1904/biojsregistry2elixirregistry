package uk.ac.ebi.tools.mapping.registry.elixir;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.annotation.Generated;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
        "usesName",
        "usesHomepage",
        "usesVersion"
})
public class Use {

    @JsonProperty("usesName")
    private String usesName;
    @JsonProperty("usesHomepage")
    private String usesHomepage;
    @JsonProperty("usesVersion")
    private String usesVersion;

    /**
     * @return The usesName
     */
    @JsonProperty("usesName")
    public String getUsesName() {
        return usesName;
    }

    /**
     * @param usesName The usesName
     */
    @JsonProperty("usesName")
    public void setUsesName(String usesName) {
        this.usesName = usesName;
    }

    /**
     * @return The usesHomepage
     */
    @JsonProperty("usesHomepage")
    public String getUsesHomepage() {
        return usesHomepage;
    }

    /**
     * @param usesHomepage The usesHomepage
     */
    @JsonProperty("usesHomepage")
    public void setUsesHomepage(String usesHomepage) {
        this.usesHomepage = usesHomepage;
    }

    /**
     * @return The usesVersion
     */
    @JsonProperty("usesVersion")
    public String getUsesVersion() {
        return usesVersion;
    }

    /**
     * @param usesVersion The usesVersion
     */
    @JsonProperty("usesVersion")
    public void setUsesVersion(String usesVersion) {
        this.usesVersion = usesVersion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Use use = (Use) o;

        if (usesHomepage != null ? !usesHomepage.equals(use.usesHomepage) : use.usesHomepage != null) return false;
        if (usesName != null ? !usesName.equals(use.usesName) : use.usesName != null) return false;
        if (usesVersion != null ? !usesVersion.equals(use.usesVersion) : use.usesVersion != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = usesName != null ? usesName.hashCode() : 0;
        result = 31 * result + (usesHomepage != null ? usesHomepage.hashCode() : 0);
        result = 31 * result + (usesVersion != null ? usesVersion.hashCode() : 0);
        return result;
    }
}
