package uk.ac.ebi.tools.mapping.registry.elixir;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.annotation.Generated;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
        "interfaceType",
        "interfaceDocs",
        "interfaceSpecURL",
        "interfaceSpecFormat"
})
public class Interface {

    @JsonProperty("interfaceType")
    private InterfaceType interfaceType;
    @JsonProperty("interfaceDocs")
    private String interfaceDocs;
    @JsonProperty("interfaceSpecURL")
    private String interfaceSpecURL;
    @JsonProperty("interfaceSpecFormat")
    private String interfaceSpecFormat;

    /**
     * @return The interfaceType
     */
    @JsonProperty("interfaceType")
    public InterfaceType getInterfaceType() {
        return interfaceType;
    }

    /**
     * @param interfaceType The interfaceType
     */
    @JsonProperty("interfaceType")
    public void setInterfaceType(InterfaceType interfaceType) {
        this.interfaceType = interfaceType;
    }

    /**
     * @return The interfaceDocs
     */
    @JsonProperty("interfaceDocs")
    public String getInterfaceDocs() {
        return interfaceDocs;
    }

    /**
     * @param interfaceDocs The interfaceDocs
     */
    @JsonProperty("interfaceDocs")
    public void setInterfaceDocs(String interfaceDocs) {
        this.interfaceDocs = interfaceDocs;
    }

    /**
     * @return The interfaceSpecURL
     */
    @JsonProperty("interfaceSpecURL")
    public String getInterfaceSpecURL() {
        return interfaceSpecURL;
    }

    /**
     * @param interfaceSpecURL The interfaceSpecURL
     */
    @JsonProperty("interfaceSpecURL")
    public void setInterfaceSpecURL(String interfaceSpecURL) {
        this.interfaceSpecURL = interfaceSpecURL;
    }

    /**
     * @return The interfaceSpecFormat
     */
    @JsonProperty("interfaceSpecFormat")
    public String getInterfaceSpecFormat() {
        return interfaceSpecFormat;
    }

    /**
     * @param interfaceSpecFormat The interfaceSpecFormat
     */
    @JsonProperty("interfaceSpecFormat")
    public void setInterfaceSpecFormat(String interfaceSpecFormat) {
        this.interfaceSpecFormat = interfaceSpecFormat;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Interface that = (Interface) o;

        if (interfaceDocs != null ? !interfaceDocs.equals(that.interfaceDocs) : that.interfaceDocs != null)
            return false;
        if (interfaceSpecFormat != null ? !interfaceSpecFormat.equals(that.interfaceSpecFormat) : that.interfaceSpecFormat != null)
            return false;
        if (interfaceSpecURL != null ? !interfaceSpecURL.equals(that.interfaceSpecURL) : that.interfaceSpecURL != null)
            return false;
        if (!interfaceType.equals(that.interfaceType)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = interfaceType.hashCode();
        result = 31 * result + (interfaceDocs != null ? interfaceDocs.hashCode() : 0);
        result = 31 * result + (interfaceSpecURL != null ? interfaceSpecURL.hashCode() : 0);
        result = 31 * result + (interfaceSpecFormat != null ? interfaceSpecFormat.hashCode() : 0);
        return result;
    }
}
