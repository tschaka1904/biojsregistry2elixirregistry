package uk.ac.ebi.tools.mapping.registry.elixir;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.annotation.Generated;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
        "docsHome",
        "docsTermsOfUse",
        "docsDownload",
        "docsCitationInstructions"
})
public class Docs {

    @JsonProperty("docsHome")
    private String docsHome;
    @JsonProperty("docsTermsOfUse")
    private String docsTermsOfUse;
    @JsonProperty("docsDownload")
    private String docsDownload;
    @JsonProperty("docsCitationInstructions")
    private String docsCitationInstructions;

    /**
     * @return The docsHome
     */
    @JsonProperty("docsHome")
    public String getDocsHome() {
        return docsHome;
    }

    /**
     * @param docsHome The docsHome
     */
    @JsonProperty("docsHome")
    public void setDocsHome(String docsHome) {
        this.docsHome = docsHome;
    }

    /**
     * @return The docsTermsOfUse
     */
    @JsonProperty("docsTermsOfUse")
    public String getDocsTermsOfUse() {
        return docsTermsOfUse;
    }

    /**
     * @param docsTermsOfUse The docsTermsOfUse
     */
    @JsonProperty("docsTermsOfUse")
    public void setDocsTermsOfUse(String docsTermsOfUse) {
        this.docsTermsOfUse = docsTermsOfUse;
    }

    /**
     * @return The docsDownload
     */
    @JsonProperty("docsDownload")
    public String getDocsDownload() {
        return docsDownload;
    }

    /**
     * @param docsDownload The docsDownload
     */
    @JsonProperty("docsDownload")
    public void setDocsDownload(String docsDownload) {
        this.docsDownload = docsDownload;
    }

    /**
     * @return The docsCitationInstructions
     */
    @JsonProperty("docsCitationInstructions")
    public String getDocsCitationInstructions() {
        return docsCitationInstructions;
    }

    /**
     * @param docsCitationInstructions The docsCitationInstructions
     */
    @JsonProperty("docsCitationInstructions")
    public void setDocsCitationInstructions(String docsCitationInstructions) {
        this.docsCitationInstructions = docsCitationInstructions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Docs docs = (Docs) o;

        if (docsCitationInstructions != null ? !docsCitationInstructions.equals(docs.docsCitationInstructions) : docs.docsCitationInstructions != null)
            return false;
        if (docsDownload != null ? !docsDownload.equals(docs.docsDownload) : docs.docsDownload != null) return false;
        if (docsHome != null ? !docsHome.equals(docs.docsHome) : docs.docsHome != null) return false;
        if (docsTermsOfUse != null ? !docsTermsOfUse.equals(docs.docsTermsOfUse) : docs.docsTermsOfUse != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = docsHome != null ? docsHome.hashCode() : 0;
        result = 31 * result + (docsTermsOfUse != null ? docsTermsOfUse.hashCode() : 0);
        result = 31 * result + (docsDownload != null ? docsDownload.hashCode() : 0);
        result = 31 * result + (docsCitationInstructions != null ? docsCitationInstructions.hashCode() : 0);
        return result;
    }
}
