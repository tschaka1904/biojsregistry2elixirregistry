package uk.ac.ebi.tools.mapping.registry.elixir;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.annotation.Generated;
import java.util.ArrayList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
        "functionName",
        "functionDescription",
        "functionHandle",
        "input",
        "output"
})
public class Function {

    @JsonProperty("functionName")
    private List<FunctionName> functionName = new ArrayList<FunctionName>();
    @JsonProperty("functionDescription")
    private String functionDescription;
    @JsonProperty("functionHandle")
    private String functionHandle;
    @JsonProperty("input")
    private List<Input> input = new ArrayList<Input>();
    @JsonProperty("output")
    private List<Output> output = new ArrayList<Output>();

    /**
     * @return The functionName
     */
    @JsonProperty("functionName")
    public List<FunctionName> getFunctionName() {
        return functionName;
    }

    /**
     * @param functionName The functionName
     */
    @JsonProperty("functionName")
    public void setFunctionName(List<FunctionName> functionName) {
        this.functionName = functionName;
    }

    /**
     * @return The functionDescription
     */
    @JsonProperty("functionDescription")
    public String getFunctionDescription() {
        return functionDescription;
    }

    /**
     * @param functionDescription The functionDescription
     */
    @JsonProperty("functionDescription")
    public void setFunctionDescription(String functionDescription) {
        this.functionDescription = functionDescription;
    }

    /**
     * @return The functionHandle
     */
    @JsonProperty("functionHandle")
    public String getFunctionHandle() {
        return functionHandle;
    }

    /**
     * @param functionHandle The functionHandle
     */
    @JsonProperty("functionHandle")
    public void setFunctionHandle(String functionHandle) {
        this.functionHandle = functionHandle;
    }

    /**
     * @return The input
     */
    @JsonProperty("input")
    public List<Input> getInput() {
        return input;
    }

    /**
     * @param input The input
     */
    @JsonProperty("input")
    public void setInput(List<Input> input) {
        this.input = input;
    }

    /**
     * @return The output
     */
    @JsonProperty("output")
    public List<Output> getOutput() {
        return output;
    }

    /**
     * @param output The output
     */
    @JsonProperty("output")
    public void setOutput(List<Output> output) {
        this.output = output;
    }
}
