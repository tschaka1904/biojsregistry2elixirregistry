package uk.ac.ebi.tools.mapping.registry.elixir;

import com.fasterxml.jackson.annotation.*;

import javax.annotation.Generated;
import java.util.*;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
        "name",
        "homepage",
        "version",
        "collection",
        "uses",
        "resourceType",
        "interface",
        "description",
        "topic",
        "tag",
        "function",
        "contact",
        "sourceRegistry",
        "maturity",
        "platform",
        "language",
        "license",
        "cost",
        "docs",
        "publications",
        "credits"
})
public class ElixirRegistryTool {

    @JsonProperty("name")
    private String name;
    @JsonProperty("homepage")
    private String homepage;
    @JsonProperty("version")
    private String version;
    @JsonProperty("collection")
    private List<String> collection = new ArrayList<String>();
    @JsonProperty("uses")
    private List<Use> uses = new ArrayList<Use>();
    @JsonProperty("resourceType")
    private List<ResourceType> resourceType = new ArrayList<ResourceType>();
    @JsonProperty("interface")
    private List<Interface> interfaces = new ArrayList<Interface>();
    @JsonProperty("description")
    private String description;
    @JsonProperty("topic")
    private List<Topic> topic = new ArrayList<Topic>();
    @JsonProperty("tag")
    private Set<Tag> tag = new HashSet<Tag>();
    @JsonProperty("function")
    private List<Function> function = new ArrayList<Function>();
    @JsonProperty("contact")
    private List<Contact> contact = new ArrayList<Contact>();
    @JsonProperty("sourceRegistry")
    private String sourceRegistry;
    @JsonProperty("maturity")
    private Maturity maturity;
    @JsonProperty("platform")
    private List<Platform> platform = new ArrayList<Platform>();
    @JsonProperty("language")
    private List<Language> language = new ArrayList<Language>();
    @JsonProperty("license")
    private License license;
    @JsonProperty("cost")
    private Cost cost;
    @JsonProperty("docs")
    private Docs docs;
    @JsonProperty("publications")
    private Publications publications;
    @JsonProperty("credits")
    private Credits credits;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public ElixirRegistryTool() {
    }

    @JsonCreator
    public ElixirRegistryTool(
            @JsonProperty("name") String name,
            @JsonProperty("homepage") String homepage,
            @JsonProperty("version") String version,
            @JsonProperty("collection") List<String> collection,
            @JsonProperty("uses") List<Use> uses,
            @JsonProperty("resourceType") List<ResourceType> resourceType,
            @JsonProperty("description") String description,
            @JsonProperty("topic") List<Topic> topic,
            @JsonProperty("tag") Set<Tag> tag,
            @JsonProperty("function") List<Function> function,
            @JsonProperty("contact") List<Contact> contact,
            @JsonProperty("sourceRegistry") String sourceRegistry,
            @JsonProperty("maturity") Maturity maturity,
            @JsonProperty("platform") List<Platform> platform,
            @JsonProperty("language") List<Language> language,
            @JsonProperty("license") License license,
            @JsonProperty("cost") Cost cost,
            @JsonProperty("docs") Docs docs,
            @JsonProperty("publications") Publications publications,
            @JsonProperty("credits") Credits credits) {
        this.name = name;
        this.homepage = homepage;
        this.version = version;
        this.collection = collection;
        this.uses = uses;
        this.resourceType = resourceType;
        this.description = description;
        this.topic = topic;
        this.tag = tag;
        this.function = function;
        this.contact = contact;
        this.sourceRegistry = sourceRegistry;
        this.maturity = maturity;
        this.platform = platform;
        this.language = language;
        this.license = license;
        this.cost = cost;
        this.docs = docs;
        this.publications = publications;
        this.credits = credits;
        this.additionalProperties = additionalProperties;
    }

    /**
     * @return The name
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The homepage
     */
    @JsonProperty("homepage")
    public String getHomepage() {
        return homepage;
    }

    /**
     * @param homepage The homepage
     */
    @JsonProperty("homepage")
    public void setHomepage(String homepage) {
        this.homepage = homepage;
    }

    /**
     * @return The version
     */
    @JsonProperty("version")
    public String getVersion() {
        return version;
    }

    /**
     * @param version The version
     */
    @JsonProperty("version")
    public void setVersion(String version) {
        this.version = version;
    }

    /**
     * @return The collection
     */
    @JsonProperty("collection")
    public List<String> getCollection() {
        return collection;
    }

    /**
     * @param collection The collection
     */
    @JsonProperty("collection")
    public void setCollection(List<String> collection) {
        this.collection = collection;
    }

    /**
     * @return The uses
     */
    @JsonProperty("uses")
    public List<Use> getUses() {
        return uses;
    }

    /**
     * @param uses The uses
     */
    @JsonProperty("uses")
    public void setUses(List<Use> uses) {
        this.uses = uses;
    }

    public List<ResourceType> getResourceType() {
        return resourceType;
    }

    public void setResourceType(List<ResourceType> resourceType) {
        this.resourceType = resourceType;
    }

    /**
     * @return The _interface
     */
    @JsonProperty("interface")
    public List<Interface> getInterface() {
        return interfaces;
    }

    /**
     * @param _interface The interface
     */
    @JsonProperty("interface")
    public void setInterface(List<Interface> _interface) {
        this.interfaces = _interface;
    }

    /**
     * @return The description
     */
    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    /**
     * @param description The description
     */
    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return The topic
     */
    @JsonProperty("topic")
    public List<Topic> getTopic() {
        return topic;
    }

    /**
     * @param topic The topic
     */
    @JsonProperty("topic")
    public void setTopic(List<Topic> topic) {
        this.topic = topic;
    }

    /**
     * @return The tag
     */
    @JsonProperty("tag")
    public Set<Tag> getTag() {
        return tag;
    }

    /**
     * @param tag The tag
     */
    @JsonProperty("tag")
    public void setTag(Set<Tag> tag) {
        this.tag = tag;
    }

    /**
     * @return The function
     */
    @JsonProperty("function")
    public List<Function> getFunction() {
        return function;
    }

    /**
     * @param function The function
     */
    @JsonProperty("function")
    public void setFunction(List<Function> function) {
        this.function = function;
    }

    /**
     * @return The contact
     */
    @JsonProperty("contact")
    public List<Contact> getContact() {
        return contact;
    }

    /**
     * @param contact The contact
     */
    @JsonProperty("contact")
    public void setContact(List<Contact> contact) {
        this.contact = contact;
    }

    /**
     * @return The sourceRegistry
     */
    @JsonProperty("sourceRegistry")
    public String getSourceRegistry() {
        return sourceRegistry;
    }

    /**
     * @param sourceRegistry The sourceRegistry
     */
    @JsonProperty("sourceRegistry")
    public void setSourceRegistry(String sourceRegistry) {
        this.sourceRegistry = sourceRegistry;
    }

    /**
     * @return The maturity
     */
    @JsonProperty("maturity")
    public Maturity getMaturity() {
        return maturity;
    }

    /**
     * @param maturity The maturity
     */
    @JsonProperty("maturity")
    public void setMaturity(Maturity maturity) {
        this.maturity = maturity;
    }

    /**
     * @return The platform
     */
    @JsonProperty("platform")
    public List<Platform> getPlatform() {
        return platform;
    }

    /**
     * @param platform The platform
     */
    @JsonProperty("platform")
    public void setPlatform(List<Platform> platform) {
        this.platform = platform;
    }

    /**
     * @return The language
     */
    @JsonProperty("language")
    public List<Language> getLanguage() {
        return language;
    }

    /**
     * @param language The language
     */
    @JsonProperty("language")
    public void setLanguage(List<Language> language) {
        this.language = language;
    }

    /**
     * @return The license
     */
    @JsonProperty("license")
    public License getLicense() {
        return license;
    }

    /**
     * @param license The license
     */
    @JsonProperty("license")
    public void setLicense(License license) {
        this.license = license;
    }

    /**
     * @return The cost
     */
    @JsonProperty("cost")
    public Cost getCost() {
        return cost;
    }

    /**
     * @param cost The cost
     */
    @JsonProperty("cost")
    public void setCost(Cost cost) {
        this.cost = cost;
    }

    /**
     * @return The docs
     */
    @JsonProperty("docs")
    public Docs getDocs() {
        return docs;
    }

    /**
     * @param docs The docs
     */
    @JsonProperty("docs")
    public void setDocs(Docs docs) {
        this.docs = docs;
    }

    /**
     * @return The publications
     */
    @JsonProperty("publications")
    public Publications getPublications() {
        return publications;
    }

    /**
     * @param publications The publications
     */
    @JsonProperty("publications")
    public void setPublications(Publications publications) {
        this.publications = publications;
    }

    /**
     * @return The credits
     */
    @JsonProperty("credits")
    public Credits getCredits() {
        return credits;
    }

    /**
     * @param credits The credits
     */
    @JsonProperty("credits")
    public void setCredits(Credits credits) {
        this.credits = credits;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ElixirRegistryTool that = (ElixirRegistryTool) o;

        if (contact != null ? !contact.equals(that.contact) : that.contact != null) return false;
        if (description != null ? !description.equals(that.description) : that.description != null) return false;
        if (homepage != null ? !homepage.equals(that.homepage) : that.homepage != null) return false;
        if (license != null ? !license.equals(that.license) : that.license != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (version != null ? !version.equals(that.version) : that.version != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (homepage != null ? homepage.hashCode() : 0);
        result = 31 * result + (version != null ? version.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (contact != null ? contact.hashCode() : 0);
        result = 31 * result + (license != null ? license.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ElixirRegistryTool{" +
                "name='" + name + '\'' +
                ", homepage='" + homepage + '\'' +
                ", version='" + version + '\'' +
                ", collection=" + collection +
                ", uses=" + uses +
                ", resourceType=" + resourceType +
                ", _interface=" + interfaces +
                ", description='" + description + '\'' +
                ", topic=" + topic +
                ", tag=" + tag +
                ", function=" + function +
                ", contact=" + contact +
                ", sourceRegistry='" + sourceRegistry + '\'' +
                ", maturity=" + maturity +
                ", platform=" + platform +
                ", language=" + language +
                ", license=" + license +
                ", cost=" + cost +
                ", docs=" + docs +
                ", publications=" + publications +
                ", credits=" + credits +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}
