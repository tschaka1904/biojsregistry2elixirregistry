package uk.ac.ebi.tools.mapping.registry.biojs;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class BioJSComponent {

    private String created;
    private String description;
    private Integer releases;
    private String version;
    private Integer versions;
    private String license;
    private String name;
    private String modified;
    private List<String> keywords;
    private Homepage homepage;
    private Author author;
    private Repository repository;

    @JsonCreator
    public BioJSComponent(
            @JsonProperty("created") String created,
            @JsonProperty("description") String description,
            @JsonProperty("releases") Integer releases,
            @JsonProperty("version") String version,
            @JsonProperty("versions") Integer versions,
            @JsonProperty("license") String license,
            @JsonProperty("name") String name,
            @JsonProperty("modified") String modified,
            @JsonProperty("keywords") List<String> keywords,
            @JsonProperty("homepage") Homepage homepage,
            @JsonProperty("author") Author author,
            @JsonProperty("repository") Repository repository) {
        this.created = created;
        this.description = description;
        this.releases = releases;
        this.version = version;
        this.versions = versions;
        this.license = license;
        this.name = name;
        this.modified = modified;
        this.keywords = keywords;
        this.homepage = homepage;
        this.author = author;
        this.repository = repository;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getReleases() {
        return releases;
    }

    public void setReleases(Integer releases) {
        this.releases = releases;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Integer getVersions() {
        return versions;
    }

    public void setVersions(Integer versions) {
        this.versions = versions;
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }

    public List<String> getKeywords() {
        return keywords;
    }

    public void setKeywords(List<String> keywords) {
        this.keywords = keywords;
    }

    public Homepage getHomepage() {
        return homepage;
    }

    public void setHomepage(Homepage homepage) {
        this.homepage = homepage;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public Repository getRepository() {
        return repository;
    }

    public void setRepository(Repository repository) {
        this.repository = repository;
    }

    @Override
    public String toString() {
        return "BioJSComponent{" +
                "created='" + created + '\'' +
                ", description='" + description + '\'' +
                ", releases=" + releases +
                ", version='" + version + '\'' +
                ", versions=" + versions +
                ", license=" + license +
                ", name='" + name + '\'' +
                ", modified='" + modified + '\'' +
                ", keywords=" + keywords +
                ", homepage=" + homepage +
                ", author=" + author +
                ", repository=" + repository +
                '}';
    }
}