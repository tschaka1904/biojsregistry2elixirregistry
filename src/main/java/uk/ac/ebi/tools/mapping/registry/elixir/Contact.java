package uk.ac.ebi.tools.mapping.registry.elixir;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.annotation.Generated;
import java.util.ArrayList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
        "contactEmail",
        "contactURL",
        "contactName",
        "contactTel",
        "contactRole"
})
public class Contact {

    @JsonProperty("contactEmail")
    private String contactEmail;
    @JsonProperty("contactURL")
    private String contactURL;
    @JsonProperty("contactName")
    private String contactName;
    @JsonProperty("contactTel")
    private String contactTel;
    @JsonProperty("contactRole")
    private List<String> contactRole = new ArrayList<String>();

    /**
     * @return The contactEmail
     */
    @JsonProperty("contactEmail")
    public String getContactEmail() {
        return contactEmail;
    }

    /**
     * @param contactEmail The contactEmail
     */
    @JsonProperty("contactEmail")
    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    /**
     * @return The contactURL
     */
    @JsonProperty("contactURL")
    public String getContactURL() {
        return contactURL;
    }

    /**
     * @param contactURL The contactURL
     */
    @JsonProperty("contactURL")
    public void setContactURL(String contactURL) {
        this.contactURL = contactURL;
    }

    /**
     * @return The contactName
     */
    @JsonProperty("contactName")
    public String getContactName() {
        return contactName;
    }

    /**
     * @param contactName The contactName
     */
    @JsonProperty("contactName")
    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    /**
     * @return The contactTel
     */
    @JsonProperty("contactTel")
    public String getContactTel() {
        return contactTel;
    }

    /**
     * @param contactTel The contactTel
     */
    @JsonProperty("contactTel")
    public void setContactTel(String contactTel) {
        this.contactTel = contactTel;
    }

    /**
     * @return The contactRole
     */
    @JsonProperty("contactRole")
    public List<String> getContactRole() {
        return contactRole;
    }

    /**
     * @param contactRole The contactRole
     */
    @JsonProperty("contactRole")
    public void setContactRole(List<String> contactRole) {
        this.contactRole = contactRole;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Contact contact = (Contact) o;

        if (!contactEmail.equals(contact.contactEmail)) return false;
        if (!contactName.equals(contact.contactName)) return false;
        if (contactRole != null ? !contactRole.equals(contact.contactRole) : contact.contactRole != null) return false;
        if (contactTel != null ? !contactTel.equals(contact.contactTel) : contact.contactTel != null) return false;
        if (contactURL != null ? !contactURL.equals(contact.contactURL) : contact.contactURL != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = contactEmail.hashCode();
        result = 31 * result + (contactURL != null ? contactURL.hashCode() : 0);
        result = 31 * result + contactName.hashCode();
        result = 31 * result + (contactTel != null ? contactTel.hashCode() : 0);
        result = 31 * result + (contactRole != null ? contactRole.hashCode() : 0);
        return result;
    }
}
