package uk.ac.ebi.tools.mapping.registry.elixir;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.annotation.Generated;
import java.util.ArrayList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
        "dataType",
        "dataFormat",
        "dataHandle"
})
public class Output {

    @JsonProperty("dataType")
    private DataType dataType;
    @JsonProperty("dataFormat")
    private List<DataFormat> dataFormat = new ArrayList<DataFormat>();
    @JsonProperty("dataHandle")
    private String dataHandle;

    /**
     * @return The dataType
     */
    @JsonProperty("dataType")
    public DataType getDataType() {
        return dataType;
    }

    /**
     * @param dataType The dataType
     */
    @JsonProperty("dataType")
    public void setDataType(DataType dataType) {
        this.dataType = dataType;
    }

    /**
     * @return The dataFormat
     */
    @JsonProperty("dataFormat")
    public List<DataFormat> getDataFormat() {
        return dataFormat;
    }

    /**
     * @param dataFormat The dataFormat
     */
    @JsonProperty("dataFormat")
    public void setDataFormat(List<DataFormat> dataFormat) {
        this.dataFormat = dataFormat;
    }

    /**
     * @return The dataHandle
     */
    @JsonProperty("dataHandle")
    public String getDataHandle() {
        return dataHandle;
    }

    /**
     * @param dataHandle The dataHandle
     */
    @JsonProperty("dataHandle")
    public void setDataHandle(String dataHandle) {
        this.dataHandle = dataHandle;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Output output = (Output) o;

        if (dataFormat != null ? !dataFormat.equals(output.dataFormat) : output.dataFormat != null) return false;
        if (dataHandle != null ? !dataHandle.equals(output.dataHandle) : output.dataHandle != null) return false;
        if (dataType != null ? !dataType.equals(output.dataType) : output.dataType != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = dataType != null ? dataType.hashCode() : 0;
        result = 31 * result + (dataFormat != null ? dataFormat.hashCode() : 0);
        result = 31 * result + (dataHandle != null ? dataHandle.hashCode() : 0);
        return result;
    }
}
