package uk.ac.ebi.tools.mapping.registry.elixir;

import com.fasterxml.jackson.annotation.*;

import javax.annotation.Generated;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
        "creditsDeveloper",
        "creditsContributor",
        "creditsInstitution",
        "creditsInfrastructure",
        "creditsFunding"
})
public class Credits {

    @JsonProperty("creditsDeveloper")
    private List<String> creditsDeveloper = new ArrayList<String>();
    @JsonProperty("creditsContributor")
    private List<String> creditsContributor = new ArrayList<String>();
    @JsonProperty("creditsInstitution")
    private List<String> creditsInstitution = new ArrayList<String>();
    @JsonProperty("creditsInfrastructure")
    private List<String> creditsInfrastructure = new ArrayList<String>();
    @JsonProperty("creditsFunding")
    private List<String> creditsFunding = new ArrayList<String>();
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * @return The creditsDeveloper
     */
    @JsonProperty("creditsDeveloper")
    public List<String> getCreditsDeveloper() {
        return creditsDeveloper;
    }

    /**
     * @param creditsDeveloper The creditsDeveloper
     */
    @JsonProperty("creditsDeveloper")
    public void setCreditsDeveloper(List<String> creditsDeveloper) {
        this.creditsDeveloper = creditsDeveloper;
    }

    /**
     * @return The creditsContributor
     */
    @JsonProperty("creditsContributor")
    public List<String> getCreditsContributor() {
        return creditsContributor;
    }

    /**
     * @param creditsContributor The creditsContributor
     */
    @JsonProperty("creditsContributor")
    public void setCreditsContributor(List<String> creditsContributor) {
        this.creditsContributor = creditsContributor;
    }

    /**
     * @return The creditsInstitution
     */
    @JsonProperty("creditsInstitution")
    public List<String> getCreditsInstitution() {
        return creditsInstitution;
    }

    /**
     * @param creditsInstitution The creditsInstitution
     */
    @JsonProperty("creditsInstitution")
    public void setCreditsInstitution(List<String> creditsInstitution) {
        this.creditsInstitution = creditsInstitution;
    }

    /**
     * @return The creditsInfrastructure
     */
    @JsonProperty("creditsInfrastructure")
    public List<String> getCreditsInfrastructure() {
        return creditsInfrastructure;
    }

    /**
     * @param creditsInfrastructure The creditsInfrastructure
     */
    @JsonProperty("creditsInfrastructure")
    public void setCreditsInfrastructure(List<String> creditsInfrastructure) {
        this.creditsInfrastructure = creditsInfrastructure;
    }

    /**
     * @return The creditsFunding
     */
    @JsonProperty("creditsFunding")
    public List<String> getCreditsFunding() {
        return creditsFunding;
    }

    /**
     * @param creditsFunding The creditsFunding
     */
    @JsonProperty("creditsFunding")
    public void setCreditsFunding(List<String> creditsFunding) {
        this.creditsFunding = creditsFunding;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Credits credits = (Credits) o;

        if (additionalProperties != null ? !additionalProperties.equals(credits.additionalProperties) : credits.additionalProperties != null)
            return false;
        if (creditsContributor != null ? !creditsContributor.equals(credits.creditsContributor) : credits.creditsContributor != null)
            return false;
        if (!creditsDeveloper.equals(credits.creditsDeveloper)) return false;
        if (creditsFunding != null ? !creditsFunding.equals(credits.creditsFunding) : credits.creditsFunding != null)
            return false;
        if (!creditsInfrastructure.equals(credits.creditsInfrastructure)) return false;
        if (!creditsInstitution.equals(credits.creditsInstitution)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = creditsDeveloper.hashCode();
        result = 31 * result + (creditsContributor != null ? creditsContributor.hashCode() : 0);
        result = 31 * result + creditsInstitution.hashCode();
        result = 31 * result + creditsInfrastructure.hashCode();
        result = 31 * result + (creditsFunding != null ? creditsFunding.hashCode() : 0);
        result = 31 * result + (additionalProperties != null ? additionalProperties.hashCode() : 0);
        return result;
    }
}
