package uk.ac.ebi.tools.mapping.registry;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.*;
import uk.ac.ebi.tools.mapping.registry.biojs.BioJSComponent;
import uk.ac.ebi.tools.mapping.registry.elixir.ElixirRegistryTool;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author Maximilian Koch <mkoch@ebi.ac.uk>
 */
public class MappingHandler {
    /**
     * Converting a JSON-String to a Java-Object
     *
     * @param jsonString JSON-String of BioJSComponents
     * @return Set of BioJS-Components
     */
    public static Set<BioJSComponent> biojsComponentJSONToObject(String jsonString) {
        Set<BioJSComponent> bioJSComponents = new HashSet<BioJSComponent>();
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        try {
            JsonNode jsonObjects = mapper.readTree(jsonString);
            for (JsonNode jsonObject : jsonObjects) {
                bioJSComponents.add(mapper.readValue(String.valueOf(jsonObject), BioJSComponent.class));
            }
        } catch (JsonGenerationException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        //System.out.println("Mapping finished " + bioJSComponents.size() + " BioJS-Components are mapped");
        return bioJSComponents;
    }

    /**
     * Converting a Java-Object to a JSON-String
     *
     * @param elixirRegistryTool ElixirRegistryTool
     * @return JSON-String of a ElixirRegistryTool
     */
    public static String convertElixirToolToJSON(ElixirRegistryTool elixirRegistryTool) {
        ObjectMapper mapper = new ObjectMapper();
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String jsonString = "";
        try {
            jsonString = ow.writeValueAsString(elixirRegistryTool);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return jsonString;
    }

    /**
     * Create a map out of all Tools from the ELIXIR-Registry using the name as key and the whole object as value
     *
     * @param jsonString JSON-String of all Tools in the ELIXIR-Registry
     * @return Map of String, ElixirRegistryTool
     */
    public static Map<String, ElixirRegistryTool> convertElixirToolJSONTOElixirToolObject(String jsonString) {
        Map<String, ElixirRegistryTool> elixirRegistryTool = new HashMap<String, ElixirRegistryTool>();
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        try {
            JsonNode jsonObjects = mapper.readTree(jsonString);
            for (JsonNode jsonObject : jsonObjects) {
                elixirRegistryTool.put(jsonObject.get("name").asText(), (mapper.readValue(String.valueOf(jsonObject), ElixirRegistryTool.class)));
            }
        } catch (JsonGenerationException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (JsonParseException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Mapping finished " + elixirRegistryTool.size() + " BioJS-Components are mapped");
        return elixirRegistryTool;
    }
}
