package uk.ac.ebi.tools.mapping.registry;

import uk.ac.ebi.tools.mapping.registry.biojs.BioJSComponent;
import uk.ac.ebi.tools.mapping.registry.elixir.*;

import java.text.Normalizer;
import java.util.*;

/**
 * @author Maximilian Koch <mkoch@ebi.ac.uk>
 */
public class ElixirRegistryToolFactory {
    /**
     * Based on all BioJS-Components by default values und all already registered BioJS-Components as ElixirRegistryTools
     * it is needed to keep the existing data from the already registered BioJS-Components.
     *
     * @param defaultElixirTools all BioJS-Components by default mapping
     * @param elixirToolMap      all registered Elixir-Registry-Tools
     * @return A Set of Elixir-Registry-Tools
     */
    public static Set<ElixirRegistryTool> getNewElixirTools(Set<ElixirRegistryTool> defaultElixirTools, Map<String, ElixirRegistryTool> elixirToolMap) {
        Set<ElixirRegistryTool> newElixirTools = new HashSet<ElixirRegistryTool>();
        for (ElixirRegistryTool defaultElixirTool : defaultElixirTools) {
            ElixirRegistryTool existingElixirTool = elixirToolMap.get(defaultElixirTool.getName());
            if (elixirToolMap.get(defaultElixirTool.getName()) != null) {
                ElixirRegistryTool newElixirTool = ElixirRegistryToolFactory.createCustomElixirRegistryTool(defaultElixirTool, existingElixirTool);
                newElixirTools.add(newElixirTool);
            } else {
                newElixirTools.add(defaultElixirTool);
            }
        }
        return newElixirTools;
    }

    public static Set<ElixirRegistryTool> createDefaultElixirTools(Set<BioJSComponent> allBioJSComponents) {
        Set<ElixirRegistryTool> defaultElixirRegistryTools = new HashSet<ElixirRegistryTool>();
        for (BioJSComponent bioJSComponent : allBioJSComponents) {
            try {
                defaultElixirRegistryTools.add(createDefaultElixirRegistryTool(bioJSComponent));
            } catch (NullPointerException e) {
                System.err.println("NullPointerException on creating " + bioJSComponent.getName() + "-Object from given JSON");
                System.err.println("Check all important information's are given (E-Mail address, Name, description etc.)");
            }
        }
        return defaultElixirRegistryTools;
    }

    /**
     * Based on BioJS-Component and default attributes, create a new Object of ElixirRegistryTool
     *
     * @param bioJSComponent BioJSComponent
     * @return ElixirRegistryTool by default values
     * @throws NullPointerException
     */
    private static ElixirRegistryTool createDefaultElixirRegistryTool(BioJSComponent bioJSComponent) throws NullPointerException {
        ElixirRegistryTool elixirRegistryTool = new ElixirRegistryTool();
        /**
         * Information based on BioJS-Component
         */
        elixirRegistryTool.setName(bioJSComponent.getName());
        elixirRegistryTool.setDescription(bioJSComponent.getDescription());
        elixirRegistryTool.setVersion(bioJSComponent.getVersion());
        elixirRegistryTool.setHomepage(bioJSComponent.getHomepage().getUrl());
        elixirRegistryTool.setContact(createElixirRegistryToolContact(bioJSComponent));
        elixirRegistryTool.setTag(createElixirRegistryToolTag(bioJSComponent));
        //elixirRegistryTool.setLicense(createElixirRegistryLicense(bioJSComponent));
        /**
         * Default information for mandatory fields on ELIXIR-Registry
         */
        elixirRegistryTool.setTopic(createElixirRegistryToolTopics());
        elixirRegistryTool.setFunction(createElixirRegistryToolFunction());
        elixirRegistryTool.setInterface(createInterfaceType());
        elixirRegistryTool.getResourceType().add(createResourceType());
        return elixirRegistryTool;
    }

    /**
     * For ElixirRegistryTools, who are already registered in the ELIXIR-Registry, it is needed to keep maybe customized
     * information. For that all the information will be saved in a new Object of ElixirRegistryTool, because all Tools
     * in the ELIXIR-Registry gonna be deleted and re-registered.
     *
     * @param defaultElixirTool  ElixirRegistryTool based on createDefaultElixirRegistryTool
     * @param existingElixirTool ElixirRegistryTool based on maybe customized information from ElixirRegistry
     * @return ElixirRegistryTool based on merge between defaultElixirTool and existingElixirTool
     */
    public static ElixirRegistryTool createCustomElixirRegistryTool(ElixirRegistryTool defaultElixirTool, ElixirRegistryTool existingElixirTool) {
        defaultElixirTool.setUses(existingElixirTool.getUses());
        defaultElixirTool.setResourceType(existingElixirTool.getResourceType());
        defaultElixirTool.setInterface(existingElixirTool.getInterface());
        defaultElixirTool.setTopic(existingElixirTool.getTopic());
        defaultElixirTool.setFunction(existingElixirTool.getFunction());
        defaultElixirTool.setSourceRegistry(existingElixirTool.getSourceRegistry());
        defaultElixirTool.setMaturity(existingElixirTool.getMaturity());
        defaultElixirTool.setPlatform(existingElixirTool.getPlatform());
        defaultElixirTool.setLanguage(existingElixirTool.getLanguage());
        defaultElixirTool.setCost(existingElixirTool.getCost());
        defaultElixirTool.setDocs(existingElixirTool.getDocs());
        defaultElixirTool.setPublications(existingElixirTool.getPublications());
        defaultElixirTool.setCredits(existingElixirTool.getCredits());
        return defaultElixirTool;
    }

    /**
     * Needed to create default ElixirRegistryTool, because of mandatory fields. Topic is using Terms of
     * EDAM-Ontology. Topic is default as top level value. Can be changed on ELIXIR-Registry interface.
     * Return as collection is needed for JSON-Schema.
     *
     * @return List of Topics
     * @throws NullPointerException
     */
    private static List<Topic> createElixirRegistryToolTopics() throws NullPointerException {
        List<Topic> topics = new ArrayList<Topic>();
        Topic topic = new Topic();
        topic.setTerm("Bioinformatics");
        topic.setUri("http://edamontology.org/topic_0091");
        topics.add(topic);
        return topics;
    }

    /**
     * Needed to create default ElixirRegistryTool, because of mandatory fields. FunctionName is using Terms of
     * EDAM-Ontology. FunctionName is default as top level value. Can be changed on ELIXIR-Registry interface.
     * Return as collection is needed for JSON-Schema.
     *
     * @return List of Function
     * @throws NullPointerException
     */
    private static List<Function> createElixirRegistryToolFunction() throws NullPointerException {
        List<Function> functions = new ArrayList<Function>();
        Function function = new Function();
        List<FunctionName> functionNames = new ArrayList<FunctionName>();
        functionNames.add(new FunctionName("Operation", "http://edamontology.org/operation_0004"));
        function.setFunctionName(functionNames);
        function.setInput(createElixirRegistryToolInput());
        function.setOutput(createElixirRegistryToolOutput());
        functions.add(function);
        return functions;
    }

    /**
     * Needed to create default ElixirRegistryTool, because of mandatory fields.
     * Return as collection is needed for JSON-Schema.
     *
     * @param bioJSComponent BioJSComponent for getting Author information
     * @return List of Contact
     * @throws NullPointerException
     */
    private static List<Contact> createElixirRegistryToolContact(BioJSComponent bioJSComponent) throws NullPointerException {
        List<Contact> contacts = new ArrayList<Contact>();
        Contact contact = new Contact();
        contact.setContactEmail(Normalizer.normalize(bioJSComponent.getAuthor().getEmail(), Normalizer.Form.NFD));
        contact.setContactName(Normalizer.normalize(bioJSComponent.getAuthor().getName(), Normalizer.Form.NFD));
        contacts.add(contact);
        return contacts;
    }

    /**
     * Needed to create default ElixirRegistryTool, because of mandatory fields. Interface is using Terms of
     * CBS-Ontology. Interface is default as top level value.
     * Return as collection is needed for JSON-Schema.
     *
     * @return List of Interface
     * @throws NullPointerException
     */
    private static List<Interface> createInterfaceType() throws NullPointerException {
        List<Interface> interfaces = new ArrayList<Interface>();
        Interface anInterface = new Interface();
        InterfaceType interfaceType = new InterfaceType("http://www.cbs.dtu.dk/ontology/interface_type/3", "WebUI");
        anInterface.setInterfaceType(interfaceType);
        interfaces.add(anInterface);
        return interfaces;
    }

    /**
     * Needed to create default ElixirRegistryTool, because of mandatory fields. ResourceType is using Terms of
     * CBS-Ontology. Can be changed on ELIXIR-Registry interface.
     * Return as collection is needed for JSON-Schema.
     *
     * @return ResourceType
     * @throws NullPointerException
     */
    private static ResourceType createResourceType() throws NullPointerException {
        ResourceType resourceType = new ResourceType();
        resourceType.setTerm("Widget");
        resourceType.setUri("http://www.cbs.dtu.dk/ontology/resource_type/12");
        return resourceType;
    }

    /**
     * Needed to create default ElixirRegistryTool, because of mandatory fields. DataType is using Terms of
     * EDAM-Ontology. DataType is default as top level value. Can be changed on ELIXIR-Registry interface.
     * Return as collection is needed for JSON-Schema.
     *
     * @return List of Input
     * @throws NullPointerException
     */
    private static List<Input> createElixirRegistryToolInput() throws NullPointerException {
        List<Input> inputs = new ArrayList<Input>();
        Input input = new Input();
        DataType dataType = new DataType();
        dataType.setTerm("Data");
        dataType.setUri("http://edamontology.org/data_0006");
        input.setDataType(dataType);
        inputs.add(input);
        return inputs;
    }

    /**
     * Needed to create default ElixirRegistryTool, because of mandatory fields. DataType is using Terms of
     * EDAM-Ontology. DataType is default as top level value. Can be changed on ELIXIR-Registry interface.
     * Return as collection is needed for JSON-Schema.
     *
     * @return List of Output
     * @throws NullPointerException
     */
    private static List<Output> createElixirRegistryToolOutput() throws NullPointerException {
        List<Output> outputs = new ArrayList<Output>();
        Output output = new Output();
        DataType dataType = new DataType();
        dataType.setTerm("Data");
        dataType.setUri("http://edamontology.org/data_0006");
        output.setDataType(dataType);
        outputs.add(output);
        return outputs;
    }

    /**
     * Based on BioJS-Components-Keywords, the ELIXIR-Registry-Tag's will be created.
     * On default every ELIXIR-Registry-Tool contains the tag "BioJS"
     *
     * @param bioJSComponent BioJSComponent for getting BioJS-Components keywords
     * @return all Set of Tag's
     * @throws NullPointerException
     */
    private static Set<Tag> createElixirRegistryToolTag(BioJSComponent bioJSComponent) throws NullPointerException {
        Set<Tag> elixirRegistryToolTags = new HashSet<Tag>();
        for (String bioJSKeyword : bioJSComponent.getKeywords()) {
            Tag tag = new Tag();
            tag.setTerm(bioJSKeyword);
            elixirRegistryToolTags.add(tag);
        }
        Tag tag = new Tag();
        tag.setTerm("BioJS");
        elixirRegistryToolTags.add(tag);
        return elixirRegistryToolTags;
    }

//    private static License createElixirRegistryLicense (BioJSComponent bioJSComponent) throws NullPointerException{
//        License license = new License();
//        license.setTerm(bioJSComponent.getLicense());
//        System.out.println(license.getTerm());
//        return license;
//    }

//    private static Set<Tag> updateElixirRegistryToolTag(BioJSComponent bioJSComponent) {
//
//        return null;
//    }
}
