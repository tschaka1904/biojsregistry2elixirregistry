package uk.ac.ebi.tools.mapping.registry.elixir;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.annotation.Generated;
import java.util.ArrayList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
        "publicationsPrimaryID",
        "publicationsOtherID"
})
public class Publications {

    @JsonProperty("publicationsPrimaryID")
    private String publicationsPrimaryID;
    @JsonProperty("publicationsOtherID")
    private List<String> publicationsOtherID = new ArrayList<String>();

    /**
     * @return The publicationsPrimaryID
     */
    @JsonProperty("publicationsPrimaryID")
    public String getPublicationsPrimaryID() {
        return publicationsPrimaryID;
    }

    /**
     * @param publicationsPrimaryID The publicationsPrimaryID
     */
    @JsonProperty("publicationsPrimaryID")
    public void setPublicationsPrimaryID(String publicationsPrimaryID) {
        this.publicationsPrimaryID = publicationsPrimaryID;
    }

    /**
     * @return The publicationsOtherID
     */
    @JsonProperty("publicationsOtherID")
    public List<String> getPublicationsOtherID() {
        return publicationsOtherID;
    }

    /**
     * @param publicationsOtherID The publicationsOtherID
     */
    @JsonProperty("publicationsOtherID")
    public void setPublicationsOtherID(List<String> publicationsOtherID) {
        this.publicationsOtherID = publicationsOtherID;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Publications that = (Publications) o;

        if (publicationsOtherID != null ? !publicationsOtherID.equals(that.publicationsOtherID) : that.publicationsOtherID != null)
            return false;
        if (publicationsPrimaryID != null ? !publicationsPrimaryID.equals(that.publicationsPrimaryID) : that.publicationsPrimaryID != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = publicationsPrimaryID != null ? publicationsPrimaryID.hashCode() : 0;
        result = 31 * result + (publicationsOtherID != null ? publicationsOtherID.hashCode() : 0);
        return result;
    }
}
